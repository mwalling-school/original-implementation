
public class EventList {

	public static final int TYPE_RANDOM_OUTAGE = 1;
	private double calendar[];
	private int firstFree;
	private int firstEvent;
	public static int NUMBER_FIELDS = 4 + 2;
	public static int CAL_FIELD_PREVIOUS = 0, ATRIB_FIELD_CLOCK = 0, ATRIB_FIELD_TYPE = 1, ATRIB_FIELD_UNIT = 2,
			ATRIB_FIELD_STATUS = 3, CAL_FIELD_NEXT = 5;

	public EventList() {
		this(10000);
	}

	public EventList(int initialSize) {
		calendar = new double[initialSize * NUMBER_FIELDS];
		firstFree = 0;
		firstEvent = -1;

		calendar[0 + CAL_FIELD_PREVIOUS] = -1;
		calendar[0 + CAL_FIELD_NEXT] = NUMBER_FIELDS;
		for (int i = 1; i < initialSize; i++) {
			calendar[(i * NUMBER_FIELDS) + CAL_FIELD_PREVIOUS] = (i - 1) * NUMBER_FIELDS;
			calendar[((i - 1) * NUMBER_FIELDS) + CAL_FIELD_NEXT] = i * NUMBER_FIELDS;
		}
	}

	public double[] getFirstEvent() {
		double[] attrib = new double[NUMBER_FIELDS - 2];
		if (firstEvent == -1) {
			return attrib;
		}
		for (int i = 0; i < NUMBER_FIELDS - 2; i++) {
			attrib[i] = calendar[firstEvent + i + 1];
		}
		int nextEvent = (int) calendar[firstEvent + CAL_FIELD_NEXT];
		calendar[firstEvent + CAL_FIELD_PREVIOUS] = -1;
		if (firstFree == -1) {
			calendar[firstEvent + CAL_FIELD_NEXT] = -1;
		} else {
			calendar[firstEvent + CAL_FIELD_NEXT] = firstFree;
		}
		firstFree = firstEvent;

		firstEvent = nextEvent;
		calendar[firstEvent + CAL_FIELD_PREVIOUS] = -1;
		return attrib;
	}

	public void addEvent(double[] attrib) {
		if (firstFree == -1) {
			throw new OutOfMemoryError("calendar cells exhausted");
		}
		int freeCell = firstFree;
		firstFree = (int) calendar[freeCell + CAL_FIELD_NEXT];
		for (int i = 0; i < attrib.length; i++) {
			calendar[freeCell + i + 1] = attrib[i];
		}
		int thisEvent = firstEvent;
		while (true) {
			if (attrib[ATRIB_FIELD_CLOCK] < calendar[thisEvent + 1 + ATRIB_FIELD_CLOCK]) {
				insertAttribBefore(thisEvent, freeCell);
				return;
			}
			int nextEvent = (int) calendar[thisEvent + CAL_FIELD_NEXT];
			if (nextEvent == -1) {
				insertAttribAfter(thisEvent, freeCell);
				return;
			}
		}
	}

	private void insertAttribAfter(int thisEvent, int freeCell) {
		calendar[thisEvent + CAL_FIELD_NEXT] = freeCell;
		calendar[freeCell + CAL_FIELD_PREVIOUS] = thisEvent;
		calendar[freeCell + CAL_FIELD_NEXT] = -1;
	}

	private void insertAttribBefore(int thisEvent, int freeCell) {
		int previousCell = (int) calendar[thisEvent + CAL_FIELD_PREVIOUS];
		if (previousCell == -1) {
			firstEvent = freeCell;
		} else {
			calendar[previousCell + CAL_FIELD_NEXT] = freeCell;
		}
		calendar[freeCell + CAL_FIELD_PREVIOUS] = previousCell;
		calendar[freeCell + CAL_FIELD_NEXT] = thisEvent;
	}
}
