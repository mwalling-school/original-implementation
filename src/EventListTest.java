import java.util.Random;

public class EventListTest {
	private static final int HOURS_IN_YEAR = 8760;

	private static int num_units = 10;
	private static double clock = 0.0;
	private static double rates[][] = new double[num_units][];
	private static int current_state[] = new int[num_units];

	private static EventList e = new EventList();
	private static Random rng = new Random();

	public static void main(String[] args) {

		for (int i = 0; i < num_units; i++) {
			// This is the annual outage rate, expressed as a per-unit of the
			// year. The offsets will cause a random rate between 5% and 15% to
			// be selected
			double outage_rate = rng.nextDouble() * .10 + .05;
			int num_outages = rng.nextInt(5) + 2;

			// set the "time in state" for the two states. The unit only moves
			// between two states
			rates[i] = new double[] { HOURS_IN_YEAR * outage_rate / num_outages,
					HOURS_IN_YEAR * (1 - outage_rate) / num_outages };

			// compute the initial state based on a random draw being less than
			// the outage rate
			current_state[i] = rng.nextDouble() < outage_rate ? 0 : 1;
			setRandomOutage(i);
		}
	}

	private static void setRandomOutage(int unit_idx) {
		// calculate the time in this state for this unit based on an
		// exponentially distributed random number around the mean time in state
		double mean_time_in_state = rates[unit_idx][current_state[unit_idx]];
		double time_in_state = Math.log(1 - rng.nextDouble()) / (-1 * mean_time_in_state);
		
		double event_time = clock + time_in_state;
		double attrib[] = new double[EventList.NUMBER_FIELDS];
		attrib[EventList.ATRIB_FIELD_CLOCK] = event_time;
		attrib[EventList.ATRIB_FIELD_STATUS] = (current_state[unit_idx] + 1) % 2;
		attrib[EventList.ATRIB_FIELD_TYPE] = EventList.TYPE_RANDOM_OUTAGE;
		attrib[EventList.ATRIB_FIELD_UNIT] = unit_idx;
		e.addEvent(attrib);
	}
}
