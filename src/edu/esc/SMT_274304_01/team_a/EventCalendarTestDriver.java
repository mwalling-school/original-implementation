package edu.esc.SMT_274304_01.team_a;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class EventCalendarTestDriver {
	private static final int NUMBER_OF_UNITS = 4;
	private static final int NUMBER_OF_CELLS = NUMBER_OF_UNITS * 3;
	private static final double CLOCK_HOUR_TO_STOP_AT = 8760.0 * 2.0;

	public static void main(String[] args) {
		Random rng = new Random(72381910394l);
		EventCalendar calendar = new EventCalendar(NUMBER_OF_CELLS);
		Set<GeneratingUnit> units = new HashSet<>();
		for (int i = 0; i < NUMBER_OF_UNITS; i++) {
			GeneratingUnit unit = new GeneratingUnit(rng, calendar, i);
			// System.out.println(calendar.toString());
			units.add(unit);
		}
		PerformCalculationsEventData calculationEvent = new PerformCalculationsEventData(0.0, calendar, 10.0);
		calendar.addEvent(calculationEvent);
		System.out.println(calendar.toString());
		EventData event = calendar.getFirstItem();
		double lastClock = -1.0;
		while (event.getClock() < CLOCK_HOUR_TO_STOP_AT) {
			event.doAction();
			// System.out.println(calendar.toString());
			event = calendar.getFirstItem();
			if (event.getClock() < lastClock) {
				System.err.println("Clock moved backwards");
				System.err.println(event);
				System.out.println(calendar.toString());
				return;
			}
			lastClock = event.getClock();

		}
	}
}
