package edu.esc.SMT_274304_01.team_a;

import java.util.function.Predicate;

public class EventCalendar {

	private Event[] eventList;
	private int topOfList;
	private int firstFreeCell;

	public EventCalendar(int listSize) {
		eventList = new Event[listSize];
		for (int i = 0; i < eventList.length; i++) {
			eventList[i] = new Event();
			eventList[i].setPredecessorIndex(i - 1);
			eventList[i].setSuccessorIndex(i + 1);
		}
		eventList[eventList.length - 1].setSuccessorIndex(-1);
		topOfList = -1;
		firstFreeCell = 0;
	}

	public void addEvent(EventData eventData) {
		// System.out.println("!!!Adding " + eventData);
		int newCellCursor = firstFreeCell;
		eventList[newCellCursor].setEventData(eventData);
		firstFreeCell = eventList[newCellCursor].getSuccessorIndex();
		eventList[firstFreeCell].setPredecessorIndex(-1);

		// No cells exist in the list
		if (topOfList == -1) {
			topOfList = newCellCursor;
			eventList[newCellCursor].setSuccessorIndex(-1);
			return;
		}

		// This cell is earlier than the top of the list
		if (eventList[newCellCursor].compareTo(eventList[topOfList]) < 0) {
			// System.out.println("!!!new cell prior to top of list, shuffling
			// top of list");
			eventList[newCellCursor].setSuccessorIndex(topOfList);
			eventList[newCellCursor].setPredecessorIndex(-1);
			eventList[topOfList].setPredecessorIndex(newCellCursor);
			topOfList = newCellCursor;
			return;
		}

		// Insert the new cell after this point, bail out of the loop when you
		// hit the last item (insert after the last item
		int targetCursor = topOfList;
		while (eventList[targetCursor].hasNext()) {
			int nextIndex = eventList[targetCursor].getSuccessorIndex();
			// is the clock of this item less than the clock of the next item?
			// if so, advance to the next item. If the next item was greater
			// than the clock, we don't want to advance since we want to insert
			// after the current item.
			// System.out.printf("==newCell:%d/%.1f,next:%d/%.1f,compare:%d\n",
			// newCellCursor,
			// eventList[newCellCursor].getEventData().getClock(), nextIndex,
			// eventList[nextIndex].getEventData().getClock(),
			// eventList[nextIndex].compareTo(eventList[newCellCursor]));
			if (eventList[nextIndex].compareTo(eventList[newCellCursor]) < 0) {
				targetCursor = nextIndex;
			} else {
				break;
			}
		}
		// System.out.println("!!!target is after " + targetCursor);
		int originalSuccessor = eventList[targetCursor].getSuccessorIndex();
		eventList[targetCursor].setSuccessorIndex(newCellCursor);
		eventList[newCellCursor].setPredecessorIndex(targetCursor);
		eventList[newCellCursor].setSuccessorIndex(originalSuccessor);
		if (originalSuccessor >= 0) {
			eventList[originalSuccessor].setPredecessorIndex(newCellCursor);
		}
	}

	private void freeItem(int targetIndex) {
		eventList[targetIndex].setEventData(null);
		eventList[targetIndex].setSuccessorIndex(firstFreeCell);
		eventList[firstFreeCell].setPredecessorIndex(targetIndex);
		firstFreeCell = targetIndex;
	}

	public EventData getFirstItem() {
		int targetCursor = topOfList;
		topOfList = eventList[targetCursor].getSuccessorIndex();
		eventList[topOfList].setPredecessorIndex(-1);
		EventData data = eventList[targetCursor].getEventData();
		freeItem(targetCursor);
		return data;
	}

	// I can't figure out why i have to use these in the Fortran version at
	// work.

	// private void deleteAtIndex(int targetIndex) {
	// int originalPredecessor = eventList[targetIndex].getPredecessorIndex();
	// int originalSuccessor = eventList[targetIndex].getSuccessorIndex();
	//
	// eventList[originalPredecessor].setSuccessorIndex(originalSuccessor);
	// eventList[originalSuccessor].setPredecessorIndex(originalPredecessor);
	//
	// freeItem(targetIndex);
	// }
	//
	// public void deleteEventsMatching(Predicate<EventData> predicate) {
	// int targetIndex = topOfList;
	// while (eventList[targetIndex].hasNext()) {
	// if (predicate.test(eventList[targetIndex].getEventData())) {
	// deleteAtIndex(targetIndex);
	// }
	// targetIndex = eventList[targetIndex].getSuccessorIndex();
	// }
	// }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(String.format("Head: %3d / Free: %3d\n", topOfList, firstFreeCell));
		for (int i = 0; i < eventList.length; i++) {
			builder.append(String.format("%03d: ", i)).append(eventList[i].toString()).append("\n");
		}
		return builder.toString();
	}

	// Where the calendar is walking looking for the comparable to be greater
	// than, we're looking for specific attributes of an item.
	public EventData findFirstEventMatching(Predicate<EventData> predicate) {
		int targetIndex = topOfList;
		while (eventList[targetIndex].hasNext()) {
			if (predicate.test(eventList[targetIndex].getEventData())) {
				return eventList[targetIndex].getEventData();
			}
			targetIndex = eventList[targetIndex].getSuccessorIndex();
		}
		return null;
	}
}
