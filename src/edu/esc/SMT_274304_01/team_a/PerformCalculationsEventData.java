package edu.esc.SMT_274304_01.team_a;

public class PerformCalculationsEventData extends EventData {

	private double timeStep;
	private EventCalendar calendarInstance;

	public PerformCalculationsEventData(double clock, EventCalendar calendarInstance, double timeStep) {
		this.setClock(clock);
		this.calendarInstance = calendarInstance;
		this.timeStep = timeStep;
	}

	@Override
	public void doAction() {
		System.out.printf("I am performing calculations at %.2f\n", getClock());
		PerformCalculationsEventData newEvent = new PerformCalculationsEventData(getClock() + timeStep,
				calendarInstance, timeStep);
		calendarInstance.addEvent(newEvent);
	}

	@Override
	public String toString() {
		return String.format("Calculation event at clock %.1f", getClock());
	}
}
