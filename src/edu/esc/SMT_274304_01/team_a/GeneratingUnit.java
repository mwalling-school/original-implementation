package edu.esc.SMT_274304_01.team_a;

import java.util.Random;

public class GeneratingUnit {

	private static final double HOURS_IN_YEAR = 8760.0;
	private Random rng;
	private double plannedOutageRate;
	private int currentState;
	private double[] rates;
	private boolean onPlannedOutage;
	private EventCalendar eventCalendar;
	private int unitIndex;
	private double plannedOutageAnnualStartTime;

	public GeneratingUnit(Random rng, EventCalendar eventCalendar, int idx) {
		this.rng = rng;
		this.eventCalendar = eventCalendar;
		this.unitIndex = idx;
		// fake random outage between 5% and 15% per year
		double randomOutageRate = rng.nextDouble() * .10 + .05;
		// fake number of outages per year between 2 and 6 (inclusive)
		float numOutages = rng.nextInt(5) + 2;
		rates = new double[] { randomOutageRate / numOutages, (1 - randomOutageRate) / numOutages };
		currentState = rng.nextDouble() < randomOutageRate ? 0 : 1;
		scheduleNextRandomOutage(0.0);

		// fake planned outage rate from 10% and 30% per year
		plannedOutageRate = rng.nextDouble() * .20 + .10;
		// fake the start time for the planned outage rate (8760 is the number
		// of hours in a non-leap year). This will be the start time of the
		// outage each year
		plannedOutageAnnualStartTime = rng.nextDouble() * HOURS_IN_YEAR;
		onPlannedOutage = false;
		scheduleNextPlannedOutage(plannedOutageAnnualStartTime);
	}

	public int getNextState() {
		return currentState == 0 ? 1 : 0;
	}

	public boolean isOnPlannedOutage() {
		return onPlannedOutage;
	}

	public boolean isOnRandomOutage() {
		return currentState == 1;
	}

	public void scheduleNextRandomOutage(double clock) {
		double time_in_state = Math.log(1 - rng.nextDouble()) / (-1 * rates[getNextState()]);
		if (clock == 0.0) {
			time_in_state = time_in_state * rng.nextDouble();
		}
		double baseClock = clock;
		if (isOnPlannedOutage()) {
			EventData returnToService = eventCalendar
					.findFirstEventMatching(e -> e.getClass().equals(GeneratorPlannedOutageEventData.class)
							&& ((GeneratorPlannedOutageEventData) e).getGeneratingUnit().equals(this));
			if (returnToService != null) {
				baseClock = returnToService.getClock();
				System.out.printf("Random outage delayed and starting at %.1f\n", baseClock);
			}
		}
		currentState = getNextState();
		GeneratorRandomOutageEventData eventData = new GeneratorRandomOutageEventData(baseClock + time_in_state, this);
		eventCalendar.addEvent(eventData);
	}

	public String getName() {
		return String.format("Unit %d", unitIndex);
	}

	public void scheduleNextPlannedOutage(double clock) {
		if (isOnPlannedOutage()) {
			// we're on a planned outage, so return to service and schedule the
			// following year's planned outage
			onPlannedOutage = false;
			double replication = Math.floor(clock / HOURS_IN_YEAR);
			double nextClock = replication * HOURS_IN_YEAR + plannedOutageAnnualStartTime;
			GeneratorPlannedOutageEventData eventData = new GeneratorPlannedOutageEventData(nextClock, this);
			eventCalendar.addEvent(eventData);
			return;
		} else {
			// we're not on a planned outage, so go on outage and schedule the
			// return to service.
			onPlannedOutage = true;
			double baseClock = clock;
			if (isOnRandomOutage()) {
				EventData returnToService = eventCalendar
						.findFirstEventMatching(e -> e.getClass().equals(GeneratorRandomOutageEventData.class)
								&& ((GeneratorRandomOutageEventData) e).getGeneratingUnit().equals(this));
				if (returnToService != null) {
					baseClock = returnToService.getClock();
					System.out.printf("Planned outage delayed and starting at %.1f\n", baseClock);
				}
			}
			double nextClock = clock + plannedOutageRate * HOURS_IN_YEAR;
			GeneratorPlannedOutageEventData eventData = new GeneratorPlannedOutageEventData(nextClock, this);
			eventCalendar.addEvent(eventData);
			return;
		}
	}
}
