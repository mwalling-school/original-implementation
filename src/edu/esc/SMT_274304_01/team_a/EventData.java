package edu.esc.SMT_274304_01.team_a;

public abstract class EventData implements Comparable<EventData> {

	private double clock;

	public abstract void doAction();

	@Override
	public int compareTo(EventData o) {
		return Double.compare(this.clock, o.clock);
	}

	@Override
	public String toString() {
		return String.format("Clock is %.1f", this.clock);
	}

	public double getClock() {
		return clock;
	}

	public void setClock(double clock) {
		this.clock = clock;
	}
}
