package edu.esc.SMT_274304_01.team_a;

public class Event implements Comparable<Event> {
	private EventData eventData;
	private int predecessorIndex;
	private int successorIndex;

	public Event() {
	}

	public int getPredecessorIndex() {
		return predecessorIndex;
	}

	public int getSuccessorIndex() {
		return successorIndex;
	}

	public void setPredecessorIndex(int predecessorIndex) {
		this.predecessorIndex = predecessorIndex;
	}

	public void setSuccessorIndex(int successorIndex) {
		this.successorIndex = successorIndex;
	}

	public EventData getEventData() {
		return eventData;
	}

	public void setEventData(EventData eventData) {
		this.eventData = eventData;
	}

	public boolean hasNext() {
		return successorIndex != -1;
	}

	@Override
	public int compareTo(Event o) {
		return eventData.compareTo(o.eventData);
	}

	@Override
	public String toString() {
		return String.format("%d <- [%s] -> %d", predecessorIndex, eventData != null ? eventData.toString() : "null",
				successorIndex);
	}
}
