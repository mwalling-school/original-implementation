package edu.esc.SMT_274304_01.team_a;

public class GeneratorRandomOutageEventData extends EventData {

	private GeneratingUnit generatingUnit;

	public GeneratorRandomOutageEventData(double clock, GeneratingUnit generatingUnit) {
		this.generatingUnit = generatingUnit;
		setClock(clock);
	}

	@Override
	public void doAction() {
		generatingUnit.scheduleNextRandomOutage(getClock());
	}

	@Override
	public String toString() {
		return String.format("Generator Random Outage for %s at %.1f", generatingUnit.getName(), getClock());
	}

	public GeneratingUnit getGeneratingUnit() {
		System.out.println(toString());
		return generatingUnit;
	}
}
